package net.minecraft.block;

import java.util.Random;

import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockObsidianLadder extends Block
{
    public BlockObsidianLadder()
    {
        super(Material.circuits);
        this.setCreativeTab(CreativeTabs.tabDecorations);
    }

    /**
     * Returns a bounding box from the pool of bounding boxes (this means this box can change after the pool has been
     * cleared to be reused)
     */
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World par1, int par2, int par3, int par4)
    {
        this.setBlockBoundsBasedOnState(par1, par2, par3, par4);
        return super.getCollisionBoundingBoxFromPool(par1, par2, par3, par4);
    }

    /**
     * Returns the bounding box of the wired rectangular prism to render.
     */
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World par1, int par2, int par3, int par4)
    {
        this.setBlockBoundsBasedOnState(par1, par2, par3, par4);
        return super.getSelectedBoundingBoxFromPool(par1, par2, par3, par4);
    }

    public void setBlockBoundsBasedOnState(IBlockAccess par1, int par2, int par3, int par4)
    {
        this.func_149797_b(par1.getBlockMetadata(par2, par3, par4));
    }

    public void func_149797_b(int par1)
    {
        float var3 = 0.125F;

        if (par1 == 2)
        {
            this.setBlockBounds(0.0F, 0.0F, 1.0F - var3, 1.0F, 1.0F, 1.0F);
        }

        if (par1 == 3)
        {
            this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, var3);
        }

        if (par1 == 4)
        {
            this.setBlockBounds(1.0F - var3, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
        }

        if (par1 == 5)
        {
            this.setBlockBounds(0.0F, 0.0F, 0.0F, var3, 1.0F, 1.0F);
        }
    }

    public boolean isOpaqueCube()
    {
        return false;
    }

    public boolean renderAsNormalBlock()
    {
        return false;
    }

    /**
     * The type of render function that is called for this block
     */
    public int getRenderType()
    {
        return 90;
    }

    public boolean canPlaceBlockAt(World par1, int par2, int par3, int par4)
    {
        return par1.getBlock(par2 - 1, par3, par4).isNormalCube() ? true : (par1.getBlock(par2 + 1, par3, par4).isNormalCube() ? true : (par1.getBlock(par2, par3, par4 - 1).isNormalCube() ? true : par1.getBlock(par2, par3, par4 + 1).isNormalCube()));
    }

    public int onBlockPlaced(World par1, int par2, int par3, int par4, int par5, float par6, float par7, float par8, int par9)
    {
        int var10 = par9;

        if ((par9 == 0 || par5 == 2) && par1.getBlock(par2, par3, par4 + 1).isNormalCube())
        {
            var10 = 2;
        }

        if ((var10 == 0 || par5 == 3) && par1.getBlock(par2, par3, par4 - 1).isNormalCube())
        {
            var10 = 3;
        }

        if ((var10 == 0 || par5 == 4) && par1.getBlock(par2 + 1, par3, par4).isNormalCube())
        {
            var10 = 4;
        }

        if ((var10 == 0 || par5 == 5) && par1.getBlock(par2 - 1, par3, par4).isNormalCube())
        {
            var10 = 5;
        }

        return var10;
    }

    public void onNeighborBlockChange(World par1, int par2, int par3, int par4, Block par5)
    {
        int var6 = par1.getBlockMetadata(par2, par3, par4);
        boolean var7 = false;

        if (var6 == 2 && par1.getBlock(par2, par3, par4 + 1).isNormalCube())
        {
            var7 = true;
        }

        if (var6 == 3 && par1.getBlock(par2, par3, par4 - 1).isNormalCube())
        {
            var7 = true;
        }

        if (var6 == 4 && par1.getBlock(par2 + 1, par3, par4).isNormalCube())
        {
            var7 = true;
        }

        if (var6 == 5 && par1.getBlock(par2 - 1, par3, par4).isNormalCube())
        {
            var7 = true;
        }

        if (!var7)
        {
            this.dropBlockAsItem(par1, par2, par3, par4, var6, 0);
            par1.setBlockToAir(par2, par3, par4);
        }

        super.onNeighborBlockChange(par1, par2, par3, par4, par5);
    }

    /**
     * Returns the quantity of items to drop on block destruction.
     */
    public int quantityDropped(Random par1)
    {
        return 1;
    }
}
