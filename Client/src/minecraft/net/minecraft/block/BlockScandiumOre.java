package net.minecraft.block;

import java.util.Random;
import net.minecraft.init.Items;
import net.minecraft.item.Item;

public class BlockScandiumOre extends BlockOre
{
    public Item getItemDropped(int p_149650_1_, Random p_149650_2_, int p_149650_3_)
    {
        return p_149650_2_.nextInt(3) == 0 ? Items.scandium_nugget : Item.getItemFromBlock(this);
    }
}
