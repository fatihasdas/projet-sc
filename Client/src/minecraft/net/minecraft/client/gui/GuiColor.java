package net.minecraft.client.gui;

import net.minecraft.util.EnumChatFormatting;

public class GuiColor {
	
	public static EnumChatFormatting c0 = EnumChatFormatting.BLACK;
	public static EnumChatFormatting c1 = EnumChatFormatting.DARK_BLUE;
	public static EnumChatFormatting c2 = EnumChatFormatting.DARK_GREEN;
	public static EnumChatFormatting c3 = EnumChatFormatting.DARK_AQUA;
	public static EnumChatFormatting c4 = EnumChatFormatting.DARK_RED;
	public static EnumChatFormatting c5 = EnumChatFormatting.DARK_PURPLE;
	public static EnumChatFormatting c6 = EnumChatFormatting.GOLD;
	public static EnumChatFormatting c7 = EnumChatFormatting.GRAY;
	public static EnumChatFormatting c8 = EnumChatFormatting.DARK_GRAY;
	public static EnumChatFormatting c9 = EnumChatFormatting.BLUE;
	public static EnumChatFormatting a = EnumChatFormatting.GREEN;
	public static EnumChatFormatting b = EnumChatFormatting.AQUA;
	public static EnumChatFormatting c = EnumChatFormatting.RED;
	public static EnumChatFormatting d = EnumChatFormatting.LIGHT_PURPLE;
	public static EnumChatFormatting e = EnumChatFormatting.YELLOW;
	public static EnumChatFormatting f = EnumChatFormatting.WHITE;
	public static EnumChatFormatting r = EnumChatFormatting.RESET;
	public static EnumChatFormatting o = EnumChatFormatting.ITALIC;
	public static EnumChatFormatting l = EnumChatFormatting.BOLD;
	public static EnumChatFormatting n = EnumChatFormatting.UNDERLINE;
	public static EnumChatFormatting m = EnumChatFormatting.STRIKETHROUGH;
	
}
