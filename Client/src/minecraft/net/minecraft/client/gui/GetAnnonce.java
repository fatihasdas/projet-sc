package net.minecraft.client.gui;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import net.minecraft.client.gui.GuiMainMenu;

public class GetAnnonce implements Runnable
{
    private GuiMainMenu main;
    private String adress;

    public GetAnnonce(GuiMainMenu par0, String par1)
    {
        this.main = par0;
        this.adress = par1;
    }

    @Override
    public void run()
    {
        String text = null;
        String tmp;
        try
        {
            URL location = new URL(this.adress); 
            BufferedReader in = null;
            in = new BufferedReader(new InputStreamReader(location.openStream())); 
            text = in.readLine();
            while ((tmp = in.readLine()) != null)
                text += "                                                        "
                	 + "                                                         " 
                	 + tmp;
            if (text != null)
                this.main.text = text; 
        }
        catch (Exception e) 
        {
            e.printStackTrace(); 
        }
    }
}