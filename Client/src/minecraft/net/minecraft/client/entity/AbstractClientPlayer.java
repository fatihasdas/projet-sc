package net.minecraft.client.entity;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.minecraft.MinecraftProfileTexture.Type;
import com.quequiere.skin.CustomSkin;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IImageBuffer;
import net.minecraft.client.renderer.ImageBufferDownload;
import net.minecraft.client.renderer.ThreadDownloadImageData;
import net.minecraft.client.renderer.texture.ITextureObject;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.resources.SkinManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.src.Config;
import net.minecraft.src.PlayerConfigurations;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StringUtils;
import net.minecraft.world.World;

import org.apache.commons.io.FilenameUtils;

public abstract class AbstractClientPlayer extends EntityPlayer implements SkinManager.SkinAvailableCallback
{
    public static final ResourceLocation locationStevePng = new ResourceLocation("textures/entity/steve.png");
    private ResourceLocation locationSkin;
    private ResourceLocation locationCape;
    private static final String __OBFID = "CL_00000935";
    private ResourceLocation ofLocationCape = null;

    public AbstractClientPlayer(World p_i45074_1_, GameProfile p_i45074_2_)
    {
        super(p_i45074_1_, p_i45074_2_);
        String var3 = this.getCommandSenderName();

        if (!var3.isEmpty())
        {
            SkinManager username = Minecraft.getMinecraft().func_152342_ad();
            username.func_152790_a(p_i45074_2_, this, true);
        }

        String username1 = p_i45074_2_.getName();
        this.downloadCape(username1);
        PlayerConfigurations.getPlayerConfiguration(this);
    }

    public boolean func_152122_n()
    {
        return !Config.isShowCapes() ? false : (this.ofLocationCape != null ? true : this.locationCape != null);
    }

    public boolean func_152123_o()
    {
        return this.locationSkin != null;
    }

    public ResourceLocation getLocationSkin()
    {
    	return CustomSkin.getSkin(this.getCommandSenderName());
    }

    public ResourceLocation getLocationCape()
    {
        return !Config.isShowCapes() ? null : (this.ofLocationCape != null ? this.ofLocationCape : this.locationCape);
    }

    public static ThreadDownloadImageData getDownloadImageSkin(ResourceLocation p_110304_0_, String p_110304_1_)
    {
        TextureManager var2 = Minecraft.getMinecraft().getTextureManager();
        Object var3 = var2.getTexture(p_110304_0_);

        if (var3 == null)
        {
            var3 = new ThreadDownloadImageData((File)null, String.format("http://scandicraft.net/skins/%s.png", new Object[] {StringUtils.stripControlCodes(p_110304_1_)}), locationStevePng, new ImageBufferDownload());
            var2.loadTexture(p_110304_0_, (ITextureObject)var3);
        }

        return (ThreadDownloadImageData)var3;
    }

    public static ResourceLocation getLocationSkin(String par0Str)
    {
        return new ResourceLocation("skins/" + StringUtils.stripControlCodes(par0Str));
    }

    public void func_152121_a(Type p_152121_1_, ResourceLocation p_152121_2_)
    {
        switch (AbstractClientPlayer.SwitchType.field_152630_a[p_152121_1_.ordinal()])
        {
            case 1:
                this.locationSkin = p_152121_2_;
                break;

            case 2:
                this.locationCape = p_152121_2_;
        }
    }

    private void downloadCape(String username)
    {
        if (username != null && !username.isEmpty())
        {
            username = StringUtils.stripControlCodes(username);
            String ofCapeUrl = "http://scandicraft.net/capes/" + username + ".png";
            String mptHash = FilenameUtils.getBaseName(ofCapeUrl);
            final ResourceLocation rl = new ResourceLocation("capeof/" + mptHash);
            TextureManager textureManager = Minecraft.getMinecraft().getTextureManager();
            ITextureObject tex = textureManager.getTexture(rl);

            if (tex != null && tex instanceof ThreadDownloadImageData)
            {
                ThreadDownloadImageData iib = (ThreadDownloadImageData)tex;

                if (iib.imageFound != null)
                {
                    if (iib.imageFound.booleanValue())
                    {
                        this.ofLocationCape = rl;
                    }

                    return;
                }
            }

            IImageBuffer iib1 = new IImageBuffer()
            {
                ImageBufferDownload ibd = new ImageBufferDownload();
                public BufferedImage parseUserSkin(BufferedImage var1)
                {
                    return AbstractClientPlayer.this.parseCape(var1);
                }
                public void func_152634_a()
                {
                    AbstractClientPlayer.this.ofLocationCape = rl;
                }
            };
            ThreadDownloadImageData textureCape = new ThreadDownloadImageData((File)null, ofCapeUrl, (ResourceLocation)null, iib1);
            textureManager.loadTexture(rl, textureCape);
        }
    }

    private BufferedImage parseCape(BufferedImage img)
    {
        int imageWidth = 64;
        int imageHeight = 32;
        int srcWidth = img.getWidth();

        for (int srcHeight = img.getHeight(); imageWidth < srcWidth || imageHeight < srcHeight; imageHeight *= 2)
        {
            imageWidth *= 2;
        }

        BufferedImage imgNew = new BufferedImage(imageWidth, imageHeight, 2);
        Graphics g = imgNew.getGraphics();
        g.drawImage(img, 0, 0, (ImageObserver)null);
        g.dispose();
        return imgNew;
    }

    static final class SwitchType
    {
        static final int[] field_152630_a = new int[Type.values().length];
        private static final String __OBFID = "CL_00001832";

        static
        {
            try
            {
                field_152630_a[Type.SKIN.ordinal()] = 1;
            }
            catch (NoSuchFieldError var2)
            {
                ;
            }

            try
            {
                field_152630_a[Type.CAPE.ordinal()] = 2;
            }
            catch (NoSuchFieldError var1)
            {
                ;
            }
        }
    }
}
