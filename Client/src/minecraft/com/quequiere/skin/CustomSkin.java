package com.quequiere.skin;

import java.awt.image.BufferedImage;
import java.util.HashMap;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.util.ResourceLocation;

public class CustomSkin 
{
	private static HashMap<String, CustomSkin> map= new  HashMap<String, CustomSkin>();
	private static final ResourceLocation steve = new ResourceLocation("textures/entity/steve.png");
	
	private String pseudo;
	private ResourceLocation rl;
	private BufferedImage buffer;
	
	public CustomSkin(String ps)
	{
		System.out.println("Create Custom skin for  "+ps);
		this.pseudo=ps;
		
		this.downloadSkin();
	}
	
	public void downloadSkin()
	{
		ImgThread it = new ImgThread(this);
		it.start();
	}
	
	public boolean hasSkin()
	{
		if(this.rl==null)
		{
			return false;
		}
		
		return true;
	}
	
	public String getPseudo()
	{
		return this.pseudo;
	}
	
	public ResourceLocation getSkin()
	{
		if(this.hasSkin())
		{
			return this.rl;
		}
		else if(this.buffer!=null)
		{
			DynamicTexture previewTexture = new DynamicTexture(this.buffer);
			TextureManager textureManager= Minecraft.getMinecraft().getTextureManager();
			ResourceLocation resourceLocation = textureManager.getDynamicTextureLocation(this.pseudo, previewTexture);
			this.rl=resourceLocation;
			
			return this.rl;
		}
		else
		{
			return steve;
		}
		
		
	}
	
	public void setBuffer(BufferedImage img)
	{
		this.buffer=img;
	}
	
	

	public static ResourceLocation getSkin(String pseudo)
	{
		if(pseudo==null||pseudo.equals("null"))
		{
			return steve;
		}
		
		CustomSkin cs = map.get(pseudo);
		
		if(cs==null)
		{
			cs = new CustomSkin(pseudo);
			map.put(pseudo,cs);
			
		}
		
		return cs.getSkin();
	}


}