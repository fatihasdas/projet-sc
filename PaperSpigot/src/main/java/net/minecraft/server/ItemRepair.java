package net.minecraft.server;

import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import net.minecraft.server.MobEffect;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.World;

public class ItemRepair extends Item {
	
	public ItemRepair()
	{
        this.maxStackSize = 1;
        this.setMaxDurability(1);
	}
   
    
    public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
        itemstack.damage(10, entityhuman);
        return itemstack;
    }

}
