package net.minecraft.server;

import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import net.minecraft.server.MobEffect;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.World;

public class ItemFalling extends Item {
	
	public ItemFalling()
	{
        this.maxStackSize = 1;
        this.setMaxDurability(1);
	}
   
    
    public ItemStack a(ItemStack itemstack, World world, EntityHuman entityhuman) {
        itemstack.damage(10, entityhuman);
        entityhuman.addEffect(new MobEffect(MobEffectList.FEATHER_FALLING.id, 6000, 1));
        return itemstack;
    }

}
