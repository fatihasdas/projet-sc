package net.minecraft.server;

public class ItemPyriteTarte extends ItemFood {

    public ItemPyriteTarte(int i, float f, boolean flag) {
        super(i, f, flag);
    }

    public EnumItemRarity f(ItemStack itemstack) {
        return itemstack.getData() == 0 ? EnumItemRarity.EPIC : EnumItemRarity.EPIC;
    }

    protected void c(ItemStack itemstack, World world, EntityHuman entityhuman) {
            if (!world.isStatic) {
                entityhuman.addEffect(new MobEffect(MobEffectList.FASTER_DIG.id, 1200, 0));
                entityhuman.addEffect(new MobEffect(MobEffectList.ABSORPTION.id, 1200, 1));
                entityhuman.addEffect(new MobEffect(MobEffectList.NIGHT_VISION.id, 3600, 0));
                entityhuman.addEffect(new MobEffect(MobEffectList.FASTER_MOVEMENT.id, 600, 1));
        } else {
            super.c(itemstack, world, entityhuman);
        }
    }
}