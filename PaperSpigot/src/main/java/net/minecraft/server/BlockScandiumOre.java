package net.minecraft.server;

import java.util.Random;

public class BlockScandiumOre extends BlockOre {

    public BlockScandiumOre() {}

    public Item getDropType(int i, Random random, int j) {
    	
        return random.nextInt(3) == 0 ? Items.scandium_nugget : Item.getItemOf(this);
    }
}