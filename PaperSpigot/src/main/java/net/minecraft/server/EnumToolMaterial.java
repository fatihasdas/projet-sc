package net.minecraft.server;

public enum EnumToolMaterial {

    WOOD("WOOD", 0, 0, 59, 2.0F, 0.0F, 15),
    STONE("STONE", 1, 1, 131, 4.0F, 1.0F, 5),
    IRON("IRON", 2, 2, 250, 6.0F, 2.0F, 14),
    EMERALD("EMERALD", 3, 3, 1561, 8.0F, 3.0F, 10),
    GOLD("GOLD", 4, 0, 32, 12.0F, 0.0F, 22),
    OBSIDIAN("OBSIDIAN", 5, 4, 2000, 13.0F, 5.0F, 10),
    lazurite("lazurite", 6, 5, 3000, 15.0F, 6.0F, 10),
    pyrite("pyrite", 7, 6, 3600, 17.0F, 7.0F, 10),
    scandium("scandium", 8, 7, 5000, 19.0F, 8.0F, 10),
    ;
    private final int f;
    private final int g;
    private final float h;
    private final float i;
    private final int j;
    private static final EnumToolMaterial[] k = new EnumToolMaterial[] { WOOD, STONE, IRON, EMERALD, GOLD, OBSIDIAN, lazurite, pyrite, scandium};

    private EnumToolMaterial(String s, int i, int j, int k, float f, float f1, int l) {
        this.f = j;
        this.g = k;
        this.h = f;
        this.i = f1;
        this.j = l;
    }

    public int a() {
        return this.g;
    }

    public float b() {
        return this.h;
    }

    public float c() {
        return this.i;
    }

    public int d() {
        return this.f;
    }

    public int e() {
        return this.j;
    }

    public Item f() {
        return 
        this == WOOD ? Item.getItemOf(Blocks.WOOD) : 
        (this == STONE ? Item.getItemOf(Blocks.COBBLESTONE) : 
        (this == GOLD ? Items.GOLD_INGOT : 
        (this == IRON ? Items.IRON_INGOT : 
        (this == EMERALD ? Items.DIAMOND : 
        (this == OBSIDIAN ? Items.obsidian_ingot : 
        (this == lazurite ? Items.lazurite : 
        (this == pyrite ? Items.pyrite : 
        (this == scandium ? Items.scandium : 
        null))))))));
    }
}

